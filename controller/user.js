const User = require('../models/user');

exports.getIndex = (req, res, next) => {
    res.render('layout', {
        path: '/'
    });
}

exports.postRegistration = (req, res, next) => {
    const email = req.body.email;
    const password = req.body.reg_password;
    const re_password = req.body.re_reg_password;
    if (password === re_password) {
        const user = new User(null, email, password);
        user.save();
        res.redirect('/');
    } else {
        console.log('a megadott jelszavak nem egyeznek!')
        res.redirect('/');
    }
};

exports.postLogin = (req, res, next) => {
    res.render('parts/modals/login', {
        path: '/'
    })
};