const path = require('path');

const express = require('express');

// TODO: controller
const userController = require('../controller/user');

const router = express.Router();

router.get('/', userController.getIndex);
router.post('/', userController.postLogin);
router.post('/', userController.postRegistration);

module.exports = router;